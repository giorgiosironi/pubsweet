# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.2.3...xpub-selectors@0.2.4) (2018-12-12)

**Note:** Version bump only for package xpub-selectors





## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.2.2...xpub-selectors@0.2.3) (2018-12-04)

**Note:** Version bump only for package xpub-selectors





## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.2.1...xpub-selectors@0.2.2) (2018-11-30)

**Note:** Version bump only for package xpub-selectors





## [0.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.2.0...xpub-selectors@0.2.1) (2018-11-29)

**Note:** Version bump only for package xpub-selectors





<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.1.0...xpub-selectors@0.2.0) (2018-11-05)


### Features

* GraphQL Xpub submit component ([ba07060](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba07060))




<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.0.5...xpub-selectors@0.1.0) (2018-05-10)


### Bug Fixes

* **components:** fix lint errors ([4e22ec1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e22ec1))
* **components:** redirect submission add selectors ([53db5a7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/53db5a7))
* **components:** review page layout ([4ea2cdd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4ea2cdd))


### Features

* **components:** add columns to submission and tabs ([40470a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/40470a0))
* **components:** add tabs to submission ([0e45892](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e45892))




<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.0.4...xpub-selectors@0.0.5) (2018-04-03)




**Note:** Version bump only for package xpub-selectors

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-selectors@0.0.3...xpub-selectors@0.0.4) (2018-03-15)




**Note:** Version bump only for package xpub-selectors

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package xpub-selectors
