# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-blog@1.0.5...pubsweet-component-blog@1.0.6) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-blog





<a name="1.0.5"></a>
## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-blog@1.0.4...pubsweet-component-blog@1.0.5) (2018-07-02)




**Note:** Version bump only for package pubsweet-component-blog

<a name="1.0.4"></a>
## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-blog@1.0.3...pubsweet-component-blog@1.0.4) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-blog

<a name="1.0.3"></a>
## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-blog@1.0.2...pubsweet-component-blog@1.0.3) (2018-03-27)


### Bug Fixes

* resolve remaining jsx-a11y lint issues ([a75c0de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a75c0de))




<a name="1.0.2"></a>
## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-blog@1.0.1...pubsweet-component-blog@1.0.2) (2018-03-05)


### Bug Fixes

* **components:** make styleguide work (mostly) ([d036681](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d036681))




<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-blog@1.0.0...pubsweet-component-blog@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-blog

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-blog@0.3.5...pubsweet-component-blog@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
