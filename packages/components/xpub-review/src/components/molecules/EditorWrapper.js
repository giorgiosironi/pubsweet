import styled from 'styled-components'

const EditorWrapper = styled.div`
  .wax-container {
    position: relative;
    .main-editor {
      margin: 3% 0 0 0;
    }
  }
`

export { EditorWrapper }
