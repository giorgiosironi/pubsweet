import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const AdminSection = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 3);
`

export default AdminSection
