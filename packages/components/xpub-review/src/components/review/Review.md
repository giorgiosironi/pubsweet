A review of a version of a project.

```js
const review = {
  comments: [{ content: 'this needs review' }],
  created: 'Thu Oct 11 2018',
  open: false,
  recommendation: 'revise',
  user: { id: 1 },
}
;<Review review={review} />
```
