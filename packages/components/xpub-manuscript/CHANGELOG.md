# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.6.3...pubsweet-component-xpub-manuscript@0.6.4) (2018-12-12)


### Bug Fixes

* **xpub-manuscript:** ensure file is present before accessing properties ([15945d4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/15945d4))





## [0.6.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.6.2...pubsweet-component-xpub-manuscript@0.6.3) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-xpub-manuscript





## [0.6.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.6.1...pubsweet-component-xpub-manuscript@0.6.2) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-xpub-manuscript





## [0.6.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.6.0...pubsweet-component-xpub-manuscript@0.6.1) (2018-11-29)

**Note:** Version bump only for package pubsweet-component-xpub-manuscript





<a name="0.6.0"></a>
# [0.6.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.7...pubsweet-component-xpub-manuscript@0.6.0) (2018-11-05)


### Features

* GraphQL Xpub review component ([66b3e73](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/66b3e73))




<a name="0.5.7"></a>
## [0.5.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.6...pubsweet-component-xpub-manuscript@0.5.7) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.5.6"></a>
## [0.5.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.5...pubsweet-component-xpub-manuscript@0.5.6) (2018-09-29)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.5.5"></a>
## [0.5.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.4...pubsweet-component-xpub-manuscript@0.5.5) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.5.4"></a>
## [0.5.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.3...pubsweet-component-xpub-manuscript@0.5.4) (2018-09-25)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.5.3"></a>
## [0.5.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.2...pubsweet-component-xpub-manuscript@0.5.3) (2018-09-20)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.5.2"></a>
## [0.5.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.1...pubsweet-component-xpub-manuscript@0.5.2) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.5.1"></a>
## [0.5.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.5.0...pubsweet-component-xpub-manuscript@0.5.1) (2018-09-06)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.5.0"></a>
# [0.5.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.4.5...pubsweet-component-xpub-manuscript@0.5.0) (2018-09-04)


### Features

* add wax-prose-mirror to xpub ([c397a97](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c397a97))




<a name="0.4.5"></a>
## [0.4.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.4.4...pubsweet-component-xpub-manuscript@0.4.5) (2018-08-23)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.4.4"></a>
## [0.4.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.4.3...pubsweet-component-xpub-manuscript@0.4.4) (2018-08-22)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.4.3"></a>
## [0.4.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.4.2...pubsweet-component-xpub-manuscript@0.4.3) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.4.2"></a>
## [0.4.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.4.1...pubsweet-component-xpub-manuscript@0.4.2) (2018-08-17)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.4.1"></a>
## [0.4.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.4.0...pubsweet-component-xpub-manuscript@0.4.1) (2018-08-02)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.4.0"></a>
# [0.4.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.3.0...pubsweet-component-xpub-manuscript@0.4.0) (2018-07-27)


### Features

* add Attachments pubsweet comp for image upload ([a2dc8ca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a2dc8ca))
* remove Wax from manuscript page ([da1147b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/da1147b))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.2.0...pubsweet-component-xpub-manuscript@0.3.0) (2018-07-19)


### Bug Fixes

* add  wax till features done ([6f9fa2c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6f9fa2c))
* linting errors ([1759e9e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1759e9e))
* wax ver 0.2.5 ([9dce588](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9dce588))


### Features

* add dependency ([6bc93e8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6bc93e8))
* add resize cursor ([15a17ca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/15a17ca))
* basic styling of the editor ([7cc9f59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7cc9f59))
* create new table command ([429d7ed](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/429d7ed))
* some styling ([e4f7f44](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e4f7f44))
* styles for table to work properly ([1defda3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1defda3))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.1.0...pubsweet-component-xpub-manuscript@0.2.0) (2018-07-12)


### Bug Fixes

* change shortcut and label text ([882a490](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/882a490))
* have wax back till all feautures are done ([615d77f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/615d77f))


### Features

* add bullet list ([f528fd0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f528fd0))
* create main editor ([b0eeca3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b0eeca3))
* join and lift lists ([505e306](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/505e306))
* ordered lists in progress ([2ba933f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2ba933f))
* replace wax with noteEditor/add basic,list schema as dependencies ([64ba50a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/64ba50a))




<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.24...pubsweet-component-xpub-manuscript@0.1.0) (2018-07-09)


### Features

* update dependency versions ([51486f4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/51486f4))




<a name="0.0.24"></a>
## [0.0.24](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.23...pubsweet-component-xpub-manuscript@0.0.24) (2018-07-03)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.23"></a>
## [0.0.23](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.22...pubsweet-component-xpub-manuscript@0.0.23) (2018-07-02)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.22"></a>
## [0.0.22](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.21...pubsweet-component-xpub-manuscript@0.0.22) (2018-06-28)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.21"></a>
## [0.0.21](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.20...pubsweet-component-xpub-manuscript@0.0.21) (2018-06-28)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.20"></a>
## [0.0.20](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.19...pubsweet-component-xpub-manuscript@0.0.20) (2018-06-19)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.19"></a>
## [0.0.19](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.18...pubsweet-component-xpub-manuscript@0.0.19) (2018-06-01)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.18"></a>
## [0.0.18](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.17...pubsweet-component-xpub-manuscript@0.0.18) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.17"></a>
## [0.0.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.16...pubsweet-component-xpub-manuscript@0.0.17) (2018-05-18)


### Bug Fixes

* use MIT on all package.json files ([4558ae4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4558ae4))




<a name="0.0.16"></a>
## [0.0.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.15...pubsweet-component-xpub-manuscript@0.0.16) (2018-05-10)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.15"></a>
## [0.0.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.14...pubsweet-component-xpub-manuscript@0.0.15) (2018-05-09)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.14"></a>
## [0.0.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.13...pubsweet-component-xpub-manuscript@0.0.14) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.13"></a>
## [0.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.12...pubsweet-component-xpub-manuscript@0.0.13) (2018-04-25)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.12"></a>
## [0.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.11...pubsweet-component-xpub-manuscript@0.0.12) (2018-04-24)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.11"></a>
## [0.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.10...pubsweet-component-xpub-manuscript@0.0.11) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.10"></a>
## [0.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.9...pubsweet-component-xpub-manuscript@0.0.10) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.9"></a>
## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.8...pubsweet-component-xpub-manuscript@0.0.9) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.8"></a>
## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.7...pubsweet-component-xpub-manuscript@0.0.8) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.6...pubsweet-component-xpub-manuscript@0.0.7) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.5...pubsweet-component-xpub-manuscript@0.0.6) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.4...pubsweet-component-xpub-manuscript@0.0.5) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-manuscript@0.0.3...pubsweet-component-xpub-manuscript@0.0.4) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-xpub-manuscript

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package pubsweet-component-xpub-manuscript
