## pubsweet-component-xpub-manuscript

A PubSweet component that provides the interface for an author to edit their manuscript.

_Note:  
To be replaced with `pubsweet-component-wax` once the issue with routing parameters is sorted out (currently Editoria uses "bookId" in routes while xpub uses "projectId" -- see [here](https://gitlab.coko.foundation/pubsweet/pubsweet-components/issues/56) for a proposal around this)._
