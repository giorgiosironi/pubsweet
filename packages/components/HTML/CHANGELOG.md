# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-html@0.2.7...pubsweet-component-html@0.2.8) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-html





<a name="0.2.7"></a>
## [0.2.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-html@0.2.6...pubsweet-component-html@0.2.7) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-html

<a name="0.2.6"></a>
## [0.2.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-html@0.2.5...pubsweet-component-html@0.2.6) (2018-03-05)


### Bug Fixes

* **components:** make styleguide work (mostly) ([d036681](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d036681))




<a name="0.2.5"></a>

## [0.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-html@0.2.4...pubsweet-component-html@0.2.5) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-html
