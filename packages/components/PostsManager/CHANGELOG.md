# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.48](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.47...pubsweet-component-posts-manager@1.0.48) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-posts-manager





## [1.0.47](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.46...pubsweet-component-posts-manager@1.0.47) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-posts-manager





## [1.0.46](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.45...pubsweet-component-posts-manager@1.0.46) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-posts-manager





## [1.0.45](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.44...pubsweet-component-posts-manager@1.0.45) (2018-11-29)

**Note:** Version bump only for package pubsweet-component-posts-manager





<a name="1.0.44"></a>
## [1.0.44](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.43...pubsweet-component-posts-manager@1.0.44) (2018-11-13)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.43"></a>
## [1.0.43](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.42...pubsweet-component-posts-manager@1.0.43) (2018-11-05)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.42"></a>
## [1.0.42](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.41...pubsweet-component-posts-manager@1.0.42) (2018-10-17)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.41"></a>
## [1.0.41](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.40...pubsweet-component-posts-manager@1.0.41) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.40"></a>
## [1.0.40](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.39...pubsweet-component-posts-manager@1.0.40) (2018-09-29)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.39"></a>
## [1.0.39](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.38...pubsweet-component-posts-manager@1.0.39) (2018-09-28)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.38"></a>
## [1.0.38](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.37...pubsweet-component-posts-manager@1.0.38) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.37"></a>
## [1.0.37](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.36...pubsweet-component-posts-manager@1.0.37) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.36"></a>
## [1.0.36](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.35...pubsweet-component-posts-manager@1.0.36) (2018-09-25)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.35"></a>
## [1.0.35](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.34...pubsweet-component-posts-manager@1.0.35) (2018-09-20)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.34"></a>
## [1.0.34](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.33...pubsweet-component-posts-manager@1.0.34) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.33"></a>
## [1.0.33](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.32...pubsweet-component-posts-manager@1.0.33) (2018-09-06)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.32"></a>
## [1.0.32](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.31...pubsweet-component-posts-manager@1.0.32) (2018-09-04)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.31"></a>
## [1.0.31](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.30...pubsweet-component-posts-manager@1.0.31) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.30"></a>
## [1.0.30](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.29...pubsweet-component-posts-manager@1.0.30) (2018-08-17)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.29"></a>
## [1.0.29](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.28...pubsweet-component-posts-manager@1.0.29) (2018-08-02)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.28"></a>
## [1.0.28](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.27...pubsweet-component-posts-manager@1.0.28) (2018-07-27)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.27"></a>
## [1.0.27](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.26...pubsweet-component-posts-manager@1.0.27) (2018-07-12)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.26"></a>
## [1.0.26](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.25...pubsweet-component-posts-manager@1.0.26) (2018-07-09)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.25"></a>
## [1.0.25](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.24...pubsweet-component-posts-manager@1.0.25) (2018-07-03)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.24"></a>
## [1.0.24](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.23...pubsweet-component-posts-manager@1.0.24) (2018-07-02)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.23"></a>
## [1.0.23](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.22...pubsweet-component-posts-manager@1.0.23) (2018-06-28)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.22"></a>
## [1.0.22](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.21...pubsweet-component-posts-manager@1.0.22) (2018-06-28)


### Bug Fixes

* **monorepo:** fix versions of ui across repo ([72ada07](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/72ada07))




<a name="1.0.21"></a>
## [1.0.21](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.20...pubsweet-component-posts-manager@1.0.21) (2018-06-19)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.20"></a>
## [1.0.20](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.19...pubsweet-component-posts-manager@1.0.20) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.19"></a>
## [1.0.19](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.18...pubsweet-component-posts-manager@1.0.19) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.18"></a>
## [1.0.18](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.17...pubsweet-component-posts-manager@1.0.18) (2018-05-10)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.17"></a>
## [1.0.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.16...pubsweet-component-posts-manager@1.0.17) (2018-05-09)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.16"></a>
## [1.0.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.15...pubsweet-component-posts-manager@1.0.16) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.15"></a>
## [1.0.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.14...pubsweet-component-posts-manager@1.0.15) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.14"></a>
## [1.0.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.13...pubsweet-component-posts-manager@1.0.14) (2018-04-25)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.13"></a>
## [1.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.12...pubsweet-component-posts-manager@1.0.13) (2018-04-24)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.12"></a>
## [1.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.11...pubsweet-component-posts-manager@1.0.12) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.11"></a>
## [1.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.10...pubsweet-component-posts-manager@1.0.11) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.10"></a>
## [1.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.9...pubsweet-component-posts-manager@1.0.10) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.9"></a>
## [1.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.8...pubsweet-component-posts-manager@1.0.9) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.8"></a>
## [1.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.7...pubsweet-component-posts-manager@1.0.8) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.7"></a>
## [1.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.6...pubsweet-component-posts-manager@1.0.7) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.6"></a>
## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.5...pubsweet-component-posts-manager@1.0.6) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.5"></a>

## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.4...pubsweet-component-posts-manager@1.0.5) (2018-03-09)

**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.4"></a>

## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.3...pubsweet-component-posts-manager@1.0.4) (2018-03-06)

**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.3"></a>

## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.2...pubsweet-component-posts-manager@1.0.3) (2018-03-05)

### Bug Fixes

* **components:** correctly redirect when edit button is clicked ([faca509](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/faca509))

<a name="1.0.2"></a>

## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.1...pubsweet-component-posts-manager@1.0.2) (2018-02-23)

**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.0...pubsweet-component-posts-manager@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@0.6.10...pubsweet-component-posts-manager@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16

<a name="0.6.10"></a>

## [0.6.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@0.6.9...pubsweet-component-posts-manager@0.6.10) (2018-02-02)

**Note:** Version bump only for package pubsweet-component-posts-manager
