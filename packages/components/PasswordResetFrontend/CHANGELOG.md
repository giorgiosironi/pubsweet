# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@3.0.2...pubsweet-component-password-reset-frontend@3.0.3) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-password-reset-frontend





## [3.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@3.0.1...pubsweet-component-password-reset-frontend@3.0.2) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-password-reset-frontend





## [3.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@3.0.0...pubsweet-component-password-reset-frontend@3.0.1) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-password-reset-frontend





# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.17...pubsweet-component-password-reset-frontend@3.0.0) (2018-11-29)


### Features

* **various:** upgrade styled-components ([9b886f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b886f6))


### BREAKING CHANGES

* **various:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="2.0.17"></a>
## [2.0.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.16...pubsweet-component-password-reset-frontend@2.0.17) (2018-11-05)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.16"></a>
## [2.0.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.15...pubsweet-component-password-reset-frontend@2.0.16) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.15"></a>
## [2.0.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.14...pubsweet-component-password-reset-frontend@2.0.15) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.14"></a>
## [2.0.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.13...pubsweet-component-password-reset-frontend@2.0.14) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.13"></a>
## [2.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.12...pubsweet-component-password-reset-frontend@2.0.13) (2018-09-06)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.12"></a>
## [2.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.11...pubsweet-component-password-reset-frontend@2.0.12) (2018-09-04)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.11"></a>
## [2.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.10...pubsweet-component-password-reset-frontend@2.0.11) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.10"></a>
## [2.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.9...pubsweet-component-password-reset-frontend@2.0.10) (2018-08-17)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.9"></a>
## [2.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.8...pubsweet-component-password-reset-frontend@2.0.9) (2018-08-02)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.8"></a>
## [2.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.7...pubsweet-component-password-reset-frontend@2.0.8) (2018-07-27)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.7"></a>
## [2.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.6...pubsweet-component-password-reset-frontend@2.0.7) (2018-07-12)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.6"></a>
## [2.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.5...pubsweet-component-password-reset-frontend@2.0.6) (2018-07-09)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.5"></a>
## [2.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.4...pubsweet-component-password-reset-frontend@2.0.5) (2018-07-03)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.4"></a>
## [2.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.3...pubsweet-component-password-reset-frontend@2.0.4) (2018-07-02)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.3"></a>
## [2.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.2...pubsweet-component-password-reset-frontend@2.0.3) (2018-06-28)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.2"></a>
## [2.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.1...pubsweet-component-password-reset-frontend@2.0.2) (2018-06-28)


### Bug Fixes

* **monorepo:** fix versions of ui across repo ([72ada07](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/72ada07))




<a name="2.0.1"></a>
## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@2.0.0...pubsweet-component-password-reset-frontend@2.0.1) (2018-06-19)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="2.0.0"></a>
# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.17...pubsweet-component-password-reset-frontend@2.0.0) (2018-06-01)


### Features

* **ui:** start ui-toolkit module ([2083b9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2083b9c))


### BREAKING CHANGES

* **ui:** th now comes from the toolkit, so all th imports from ui are now broken




<a name="1.0.17"></a>
## [1.0.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.16...pubsweet-component-password-reset-frontend@1.0.17) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.16"></a>
## [1.0.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.15...pubsweet-component-password-reset-frontend@1.0.16) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.15"></a>
## [1.0.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.14...pubsweet-component-password-reset-frontend@1.0.15) (2018-05-10)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.14"></a>
## [1.0.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.13...pubsweet-component-password-reset-frontend@1.0.14) (2018-05-09)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.13"></a>
## [1.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.12...pubsweet-component-password-reset-frontend@1.0.13) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.12"></a>
## [1.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.11...pubsweet-component-password-reset-frontend@1.0.12) (2018-04-24)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.11"></a>
## [1.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.10...pubsweet-component-password-reset-frontend@1.0.11) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.10"></a>
## [1.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.9...pubsweet-component-password-reset-frontend@1.0.10) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.9"></a>
## [1.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.8...pubsweet-component-password-reset-frontend@1.0.9) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.8"></a>
## [1.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.7...pubsweet-component-password-reset-frontend@1.0.8) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.7"></a>
## [1.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.6...pubsweet-component-password-reset-frontend@1.0.7) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.6"></a>
## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.5...pubsweet-component-password-reset-frontend@1.0.6) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.5"></a>
## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.4...pubsweet-component-password-reset-frontend@1.0.5) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.4"></a>

## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.3...pubsweet-component-password-reset-frontend@1.0.4) (2018-03-09)

**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.3"></a>

## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.2...pubsweet-component-password-reset-frontend@1.0.3) (2018-03-06)

**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.2"></a>

## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.1...pubsweet-component-password-reset-frontend@1.0.2) (2018-03-05)

### Bug Fixes

* **components:** PasswordReset was still using a CSS variable ([e1c2c84](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e1c2c84))

<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@1.0.0...pubsweet-component-password-reset-frontend@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-password-reset-frontend

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-password-reset-frontend@0.2.3...pubsweet-component-password-reset-frontend@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
