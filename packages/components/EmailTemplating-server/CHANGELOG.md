# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 0.1.0 (2018-12-12)


### Features

* add email templating component ([4baa6e0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4baa6e0))
