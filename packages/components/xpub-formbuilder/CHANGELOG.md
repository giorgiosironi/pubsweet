# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@1.0.2...pubsweet-component-xpub-formbuilder@1.0.3) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-xpub-formbuilder





## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@1.0.1...pubsweet-component-xpub-formbuilder@1.0.2) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-xpub-formbuilder





## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@1.0.0...pubsweet-component-xpub-formbuilder@1.0.1) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-xpub-formbuilder





# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@0.2.0...pubsweet-component-xpub-formbuilder@1.0.0) (2018-11-29)


### Features

* **various:** update styled-components ([5c51466](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5c51466))
* **various:** upgrade styled-components ([9b886f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b886f6))


### BREAKING CHANGES

* **various:** Replace all styled-components .extend with styled()
* **various:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@0.1.5...pubsweet-component-xpub-formbuilder@0.2.0) (2018-11-05)


### Features

* GraphQL Login component ([70df3de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/70df3de))




<a name="0.1.5"></a>
## [0.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@0.1.4...pubsweet-component-xpub-formbuilder@0.1.5) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-xpub-formbuilder

<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@0.1.3...pubsweet-component-xpub-formbuilder@0.1.4) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-xpub-formbuilder

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@0.1.2...pubsweet-component-xpub-formbuilder@0.1.3) (2018-09-25)




**Note:** Version bump only for package pubsweet-component-xpub-formbuilder

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@0.1.1...pubsweet-component-xpub-formbuilder@0.1.2) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-xpub-formbuilder

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-formbuilder@0.1.0...pubsweet-component-xpub-formbuilder@0.1.1) (2018-09-06)




**Note:** Version bump only for package pubsweet-component-xpub-formbuilder

<a name="0.1.0"></a>
# 0.1.0 (2018-09-04)


### Bug Fixes

* **fomrbuilder:** fix validation ([98b3b5e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98b3b5e))
* **formbuilder:** create folder from scratch ([a2d533e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a2d533e))
* **formbuilder:** erase files uneeded ([9bc04cd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9bc04cd))
* **formbuilder:** test addition ([1379895](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1379895))
* **readme:** change readme files ([eb0ff9b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/eb0ff9b))
* **styleguide:** remove console ([c8f7bb2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c8f7bb2))
* **test:** add data-test-id to tabs ([e8a42cb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e8a42cb))
* **test:** fix intgration test ([900f0db](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/900f0db))
* **update:** update dependencies ([52c0002](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/52c0002))
* **xpubformbuilder:** integration test and component ([6806f81](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6806f81))


### Features

* **backend:** add backend check path floder ([8c95e72](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8c95e72))
* **components:** form builder backend requests ([7082b0f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7082b0f))
* **formbuilder:** add components for the builder ([cfae1c0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cfae1c0))
* **formbuilder:** add form redux actions formbuilder ([7436353](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7436353))
* **formbuilder:** add formbuilder component ([c24b9f7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c24b9f7))
* **formbuilder:** add forms layout ([0cd6b9d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0cd6b9d))
* **formbuilder:** add validation for elements ([882935a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/882935a))
* **formbuilder:** layout of form builder ([2bd4956](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2bd4956))




# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.
