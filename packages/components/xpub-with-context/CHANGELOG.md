# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.3...xpub-with-context@0.1.4) (2018-12-12)

**Note:** Version bump only for package xpub-with-context





## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.2...xpub-with-context@0.1.3) (2018-12-04)

**Note:** Version bump only for package xpub-with-context





## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.1...xpub-with-context@0.1.2) (2018-11-30)

**Note:** Version bump only for package xpub-with-context





## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-with-context@0.1.0...xpub-with-context@0.1.1) (2018-11-29)

**Note:** Version bump only for package xpub-with-context





<a name="0.1.0"></a>
# 0.1.0 (2018-11-05)


### Features

* GraphQL Xpub submit component ([ba07060](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba07060))




# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.
