# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@4.0.2...pubsweet-component-xpub-dashboard@4.0.3) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-xpub-dashboard





## [4.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@4.0.1...pubsweet-component-xpub-dashboard@4.0.2) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-xpub-dashboard





## [4.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@4.0.0...pubsweet-component-xpub-dashboard@4.0.1) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-xpub-dashboard





# [4.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.1.0...pubsweet-component-xpub-dashboard@4.0.0) (2018-11-29)


### Features

* **various:** update styled-components ([5c51466](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5c51466))
* **various:** upgrade styled-components ([9b886f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b886f6))


### BREAKING CHANGES

* **various:** Replace all styled-components .extend with styled()
* **various:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="3.1.0"></a>
# [3.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.15...pubsweet-component-xpub-dashboard@3.1.0) (2018-11-05)


### Features

* GraphQL Login component ([70df3de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/70df3de))
* GraphQL Xpub review component ([66b3e73](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/66b3e73))
* GraphQL Xpub submit component ([ba07060](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba07060))




<a name="3.0.15"></a>
## [3.0.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.14...pubsweet-component-xpub-dashboard@3.0.15) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.14"></a>
## [3.0.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.13...pubsweet-component-xpub-dashboard@3.0.14) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.13"></a>
## [3.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.12...pubsweet-component-xpub-dashboard@3.0.13) (2018-09-25)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.12"></a>
## [3.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.11...pubsweet-component-xpub-dashboard@3.0.12) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.11"></a>
## [3.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.10...pubsweet-component-xpub-dashboard@3.0.11) (2018-09-06)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.10"></a>
## [3.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.9...pubsweet-component-xpub-dashboard@3.0.10) (2018-09-04)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.9"></a>
## [3.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.8...pubsweet-component-xpub-dashboard@3.0.9) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.8"></a>
## [3.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.7...pubsweet-component-xpub-dashboard@3.0.8) (2018-08-17)


### Bug Fixes

* **actions:** pubsweet ui responsive ([b1cab9a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b1cab9a))
* **actions:** validationStatus fix ([762432f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/762432f))
* **css:** fix responsiveness of actions ([9bff385](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9bff385))
* **revert:** valildateStatus ([5d6f53e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5d6f53e))
* **style:** remove  enter line ([e2de927](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e2de927))
* **style:** responsive line tool ([c3219ec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c3219ec))
* **warnings:** don't pass every prop to Dom Element ([d8f5e93](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d8f5e93))
* **warnings:** key actions ([2f176f0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2f176f0))
* **warnings:** naming changes ([e4727ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e4727ad))
* **warnings:** remove key from unneeded component ([2dda7a5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2dda7a5))




<a name="3.0.7"></a>
## [3.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.6...pubsweet-component-xpub-dashboard@3.0.7) (2018-08-02)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.6"></a>
## [3.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.5...pubsweet-component-xpub-dashboard@3.0.6) (2018-07-27)


### Bug Fixes

* **dashboard:** delete associated fragments ([f95c292](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f95c292))




<a name="3.0.5"></a>
## [3.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.4...pubsweet-component-xpub-dashboard@3.0.5) (2018-07-23)


### Bug Fixes

* **dashboard:** add keys to dashboard ([38f73f7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/38f73f7))
* **dashobard:** add key to compoent ([bc76925](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bc76925))




<a name="3.0.4"></a>
## [3.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.3...pubsweet-component-xpub-dashboard@3.0.4) (2018-07-19)


### Bug Fixes

* **dashboard:** fixing typo message upload ([3fec4ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3fec4ef))




<a name="3.0.3"></a>
## [3.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.2...pubsweet-component-xpub-dashboard@3.0.3) (2018-07-12)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.2"></a>
## [3.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.1...pubsweet-component-xpub-dashboard@3.0.2) (2018-07-09)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.1"></a>
## [3.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@3.0.0...pubsweet-component-xpub-dashboard@3.0.1) (2018-07-03)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="3.0.0"></a>
# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@2.0.1...pubsweet-component-xpub-dashboard@3.0.0) (2018-07-02)


### Features

* **ui:** introduce more line height variables ([85c24e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85c24e2))


### BREAKING CHANGES

* **ui:** the existing fontLineHeight variable is gone and replaced by multiple new variables




<a name="2.0.1"></a>
## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@2.0.0...pubsweet-component-xpub-dashboard@2.0.1) (2018-06-28)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="2.0.0"></a>
# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@1.0.1...pubsweet-component-xpub-dashboard@2.0.0) (2018-06-28)


### Bug Fixes

* **authorize:** add authorize to hide delete ([125a872](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/125a872))
* **authsome:** update stucture of currentUpdate ([909b80f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/909b80f))
* **components:** prevent Delete when paper is submitted status ([b556e25](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b556e25))
* **dashboard:** make use of authorize ([ccc993c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ccc993c))
* **dashboard:** mock authorize component ([198f95a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/198f95a))
* **styleguide:** correct require path ([79a2b07](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/79a2b07))


### Code Refactoring

* **ui:** replace current gridunit variables with one small value ([cf48f29](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cf48f29))


### BREAKING CHANGES

* **ui:** Your ui components will now be multiplying a much smaller value so they need to be
adjusted




<a name="1.0.1"></a>
## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@1.0.0...pubsweet-component-xpub-dashboard@1.0.1) (2018-06-19)


### Bug Fixes

* **dashboard:** add actions to dashboard editorItem ([58733b6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/58733b6))
* **dashboard:** empty declaration object ([919bde4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/919bde4))
* **metadata:** empty values ([e6f55ea](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e6f55ea))
* **pubsweet-ui:** tests are failing ([0e57798](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e57798))




<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.3.2...pubsweet-component-xpub-dashboard@1.0.0) (2018-06-01)


### Bug Fixes

* **components:** dasboard fixing multiple submissions ([01fa2f9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/01fa2f9))
* **dashboard:** empty dashboard collections ([3f4db98](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3f4db98))
* **dashboard:** remove regenerate import and add it to styleguide ([96731cf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/96731cf))
* **dashboard:** section hide on empty ([7a139ec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7a139ec))
* **dashboard:** test change object dashboard ([906ccfd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/906ccfd))
* **styleguide:** compile authsome ([8e9407f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8e9407f))
* **test:** dashboard - reviewer test ([30f41b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/30f41b3))


### Features

* **components:** add authsome to dashboard ([833a9de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/833a9de))
* **ui:** start ui-toolkit module ([2083b9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2083b9c))


### BREAKING CHANGES

* **ui:** th now comes from the toolkit, so all th imports from ui are now broken




<a name="0.3.2"></a>
## [0.3.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.3.1...pubsweet-component-xpub-dashboard@0.3.2) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.3.1"></a>
## [0.3.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.3.0...pubsweet-component-xpub-dashboard@0.3.1) (2018-05-18)


### Bug Fixes

* **components:** upload diasble during converting ([227b136](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/227b136))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.4...pubsweet-component-xpub-dashboard@0.3.0) (2018-05-10)


### Bug Fixes

* **components:** dashboard if statment reject ([999587a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/999587a))
* **components:** linter ([9aac3fa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9aac3fa))
* **components:** redirect submission add selectors ([53db5a7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/53db5a7))


### Features

* **components:** create accordion component ([54f5b7d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/54f5b7d))




<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.3...pubsweet-component-xpub-dashboard@0.2.4) (2018-05-09)


### Bug Fixes

* **xpub-dash:** fix reviewer item crash when status is revision ([fc79496](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc79496))




<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.2...pubsweet-component-xpub-dashboard@0.2.3) (2018-05-03)


### Bug Fixes

* **xpub-dashboard:** correct styles for author manuscripts ([1d8761e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1d8761e))




<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.1...pubsweet-component-xpub-dashboard@0.2.2) (2018-05-03)


### Bug Fixes

* **components:** load all users to control panel ([90c88e6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/90c88e6))
* **components:** remove from Dashboard assign editor ([751a63e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/751a63e))




<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.2.0...pubsweet-component-xpub-dashboard@0.2.1) (2018-04-24)


### Bug Fixes

* **components:** add file streamlined data ([9dd6797](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9dd6797))
* **components:** add metadata StreamLined ([29a1fcd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/29a1fcd))
* **components:** add subinfo to upload ([446fc16](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/446fc16))
* **components:** change order ([2020d49](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2020d49))
* **components:** change text to create submission button ([d3b6385](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d3b6385))
* **components:** statuses changed for revision ([3bc09dc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3bc09dc))
* **dashboard:** change stremlined metadata label ([992cc4f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/992cc4f))




<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.3...pubsweet-component-xpub-dashboard@0.2.0) (2018-04-11)


### Bug Fixes

* **components:** change title styling ([3c154ba](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3c154ba))


### Features

* **components:** add Link to control panel ([85458b9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85458b9))
* **components:** fix import add link ([dfe4818](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dfe4818))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.2...pubsweet-component-xpub-dashboard@0.1.3) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.1...pubsweet-component-xpub-dashboard@0.1.2) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.0...pubsweet-component-xpub-dashboard@0.1.1) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.5...pubsweet-component-xpub-dashboard@0.1.0) (2018-03-27)


### Features

* **styleguide:** page per section ([0bf0836](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0bf0836))




<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.4...pubsweet-component-xpub-dashboard@0.0.5) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.3...pubsweet-component-xpub-dashboard@0.0.4) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package pubsweet-component-xpub-dashboard
