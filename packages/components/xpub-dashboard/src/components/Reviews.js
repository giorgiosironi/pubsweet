import React from 'react'
import styled from 'styled-components'
import { compose } from 'recompose'
import { sumBy } from 'lodash'
import { getUserFromTeam } from 'xpub-selectors'
import { withJournal } from 'xpub-journal'
import { Badge } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

const Root = styled.div`
  display: inline-flex;
  justify-content: flex-end;
  margin-bottom: 0.6em;
  margin-top: 0.3em;
  padding-left: 1.5em;

  font-family: ${th('fontReviewer')};
  font-size: 0.9em;
`

const BadgeContainer = styled.span`
  &:not(:last-child) {
    margin-right: 10px;
  }
`

const countStatus = (version, status) => {
  const teamMember = getUserFromTeam(version, 'reviewer')

  if (status === 'rejected' || status === 'invited') {
    return sumBy(teamMember, member => (member.status === status ? 1 : 0))
  }

  if (status === 'accepted') {
    return sumBy(version.reviews, review => (review.recommendation ? 0 : 1))
  }

  if (status === 'completed') {
    return sumBy(version.reviews, review => (review.recommendation ? 1 : 0))
  }

  return 0
}

const Reviews = ({ version, journal }) => (
  <Root>
    {journal.reviewStatus.map(status => (
      <BadgeContainer key={status}>
        <Badge count={countStatus(version, status)} label={status} />
      </BadgeContainer>
    ))}
  </Root>
)

export default compose(withJournal)(Reviews)
