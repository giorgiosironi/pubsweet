# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@1.0.2...pubsweet-component-xpub-teams-manager@1.0.3) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-xpub-teams-manager





## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@1.0.1...pubsweet-component-xpub-teams-manager@1.0.2) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-xpub-teams-manager





## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@1.0.0...pubsweet-component-xpub-teams-manager@1.0.1) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-xpub-teams-manager





# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@0.2.0...pubsweet-component-xpub-teams-manager@1.0.0) (2018-11-29)


### Features

* **various:** upgrade styled-components ([9b886f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b886f6))


### BREAKING CHANGES

* **various:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@0.1.4...pubsweet-component-xpub-teams-manager@0.2.0) (2018-11-05)


### Features

* GraphQL Login component ([70df3de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/70df3de))




<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@0.1.3...pubsweet-component-xpub-teams-manager@0.1.4) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-xpub-teams-manager

<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@0.1.2...pubsweet-component-xpub-teams-manager@0.1.3) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-xpub-teams-manager

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@0.1.1...pubsweet-component-xpub-teams-manager@0.1.2) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-xpub-teams-manager

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-teams-manager@0.1.0...pubsweet-component-xpub-teams-manager@0.1.1) (2018-08-02)




**Note:** Version bump only for package pubsweet-component-xpub-teams-manager

<a name="0.1.0"></a>
# 0.1.0 (2018-07-09)


### Bug Fixes

* **components:** button to styledButton ([0404203](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0404203))
* **menu:** menu component of layout ([19bebef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/19bebef))
* **menu:** reset function ([2961a85](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2961a85))
* **teamanager:** remove name ([60cd6ee](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/60cd6ee))
* **teammanager:** test fix failing ([c559488](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c559488))
* **teammanager:** update styles component ([c92bbd5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c92bbd5))
* **teams:** update styleguide ([427bcc6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/427bcc6))
* **teams:** update ui ([23f49d5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/23f49d5))
* **test:** mock authorize ([9c89540](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9c89540))
* **test:** update snapshot ([8cd93a4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8cd93a4))
* **whitespaces:** remove whitespaces ([a4803cb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a4803cb))
* **xpub-team-manager:** move files to components ([4421a68](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4421a68))


### Features

* **team-management:** create new team component ([1f4c677](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1f4c677))
* **team-management:** fix page layout ([9f5c6b5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9f5c6b5))




# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.
