# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.30](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.29...pubsweet-component-teams-manager@1.1.30) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-teams-manager





## [1.1.29](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.28...pubsweet-component-teams-manager@1.1.29) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-teams-manager





## [1.1.28](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.27...pubsweet-component-teams-manager@1.1.28) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-teams-manager





## [1.1.27](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.26...pubsweet-component-teams-manager@1.1.27) (2018-11-29)

**Note:** Version bump only for package pubsweet-component-teams-manager





<a name="1.1.26"></a>
## [1.1.26](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.25...pubsweet-component-teams-manager@1.1.26) (2018-11-05)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.25"></a>
## [1.1.25](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.24...pubsweet-component-teams-manager@1.1.25) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.24"></a>
## [1.1.24](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.23...pubsweet-component-teams-manager@1.1.24) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.23"></a>
## [1.1.23](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.22...pubsweet-component-teams-manager@1.1.23) (2018-09-25)


### Bug Fixes

* **components:** make team manager resitant to undefined objects ([f180348](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f180348))




<a name="1.1.22"></a>
## [1.1.22](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.21...pubsweet-component-teams-manager@1.1.22) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.21"></a>
## [1.1.21](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.20...pubsweet-component-teams-manager@1.1.21) (2018-09-06)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.20"></a>
## [1.1.20](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.19...pubsweet-component-teams-manager@1.1.20) (2018-09-04)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.19"></a>
## [1.1.19](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.18...pubsweet-component-teams-manager@1.1.19) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.18"></a>
## [1.1.18](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.17...pubsweet-component-teams-manager@1.1.18) (2018-08-17)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.17"></a>
## [1.1.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.16...pubsweet-component-teams-manager@1.1.17) (2018-08-02)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.16"></a>
## [1.1.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.15...pubsweet-component-teams-manager@1.1.16) (2018-07-27)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.15"></a>
## [1.1.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.14...pubsweet-component-teams-manager@1.1.15) (2018-07-19)


### Bug Fixes

* **teams-manager:** update to fit new server Team model ([ccef37c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ccef37c))




<a name="1.1.14"></a>
## [1.1.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.13...pubsweet-component-teams-manager@1.1.14) (2018-07-12)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.13"></a>
## [1.1.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.12...pubsweet-component-teams-manager@1.1.13) (2018-07-09)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.12"></a>
## [1.1.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.11...pubsweet-component-teams-manager@1.1.12) (2018-07-03)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.11"></a>
## [1.1.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.10...pubsweet-component-teams-manager@1.1.11) (2018-07-02)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.10"></a>
## [1.1.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.9...pubsweet-component-teams-manager@1.1.10) (2018-06-28)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.9"></a>
## [1.1.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.8...pubsweet-component-teams-manager@1.1.9) (2018-06-28)


### Bug Fixes

* **monorepo:** fix versions of ui across repo ([72ada07](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/72ada07))




<a name="1.1.8"></a>
## [1.1.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.7...pubsweet-component-teams-manager@1.1.8) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.7"></a>
## [1.1.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.6...pubsweet-component-teams-manager@1.1.7) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.6"></a>
## [1.1.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.5...pubsweet-component-teams-manager@1.1.6) (2018-05-10)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.5"></a>
## [1.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.4...pubsweet-component-teams-manager@1.1.5) (2018-05-09)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.4"></a>
## [1.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.3...pubsweet-component-teams-manager@1.1.4) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.3"></a>
## [1.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.2...pubsweet-component-teams-manager@1.1.3) (2018-04-24)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.2"></a>
## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.1...pubsweet-component-teams-manager@1.1.2) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.1"></a>
## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.1.0...pubsweet-component-teams-manager@1.1.1) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.1.0"></a>
# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.8...pubsweet-component-teams-manager@1.1.0) (2018-03-30)


### Features

* **components:** remove react-bootstrap ([e66c933](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e66c933))




<a name="1.0.8"></a>
## [1.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.7...pubsweet-component-teams-manager@1.0.8) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.7"></a>
## [1.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.6...pubsweet-component-teams-manager@1.0.7) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.6"></a>
## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.5...pubsweet-component-teams-manager@1.0.6) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.5"></a>
## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.4...pubsweet-component-teams-manager@1.0.5) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.4"></a>

## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.3...pubsweet-component-teams-manager@1.0.4) (2018-03-09)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.3"></a>

## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.2...pubsweet-component-teams-manager@1.0.3) (2018-03-06)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.2"></a>

## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.1...pubsweet-component-teams-manager@1.0.2) (2018-03-05)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@1.0.0...pubsweet-component-teams-manager@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-teams-manager

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-teams-manager@0.3.2...pubsweet-component-teams-manager@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
