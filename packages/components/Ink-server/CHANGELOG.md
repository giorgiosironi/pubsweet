# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.14...pubsweet-component-ink-backend@0.2.15) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-ink-backend





<a name="0.2.14"></a>
## [0.2.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.13...pubsweet-component-ink-backend@0.2.14) (2018-09-25)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.13"></a>
## [0.2.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.12...pubsweet-component-ink-backend@0.2.13) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.12"></a>
## [0.2.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.11...pubsweet-component-ink-backend@0.2.12) (2018-08-17)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.11"></a>
## [0.2.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.10...pubsweet-component-ink-backend@0.2.11) (2018-06-19)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.10"></a>
## [0.2.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.9...pubsweet-component-ink-backend@0.2.10) (2018-06-01)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.9"></a>
## [0.2.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.8...pubsweet-component-ink-backend@0.2.9) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.8"></a>
## [0.2.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.7...pubsweet-component-ink-backend@0.2.8) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.7"></a>
## [0.2.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.6...pubsweet-component-ink-backend@0.2.7) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.6"></a>
## [0.2.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.5...pubsweet-component-ink-backend@0.2.6) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-ink-backend

<a name="0.2.5"></a>

## [0.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.4...pubsweet-component-ink-backend@0.2.5) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-ink-backend
