# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.0.9"></a>
## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.8...@pubsweet/component-polling-server@0.0.9) (2018-10-17)


### Bug Fixes

* sync cache added ([6989dd6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6989dd6))




<a name="0.0.8"></a>
## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.7...@pubsweet/component-polling-server@0.0.8) (2018-09-25)




**Note:** Version bump only for package @pubsweet/component-polling-server

<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.6...@pubsweet/component-polling-server@0.0.7) (2018-07-27)




**Note:** Version bump only for package @pubsweet/component-polling-server

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.5...@pubsweet/component-polling-server@0.0.6) (2018-06-19)


### Bug Fixes

* **polling server:** locking and unlocking handled in server now ([19ac6bf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/19ac6bf))




<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.4...@pubsweet/component-polling-server@0.0.5) (2018-04-03)




**Note:** Version bump only for package @pubsweet/component-polling-server

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.3...@pubsweet/component-polling-server@0.0.4) (2018-03-27)


### Bug Fixes

* **polling-server:** remove revs ([b42cecd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b42cecd))




<a name="0.0.3"></a>
## [0.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-polling-server@0.0.2...@pubsweet/component-polling-server@0.0.3) (2018-03-19)




**Note:** Version bump only for package @pubsweet/component-polling-server
