# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.2.2"></a>
## [1.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.2.1...@pubsweet/component-aws-s3@1.2.2) (2018-09-25)




**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="1.2.1"></a>
## [1.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.2.0...@pubsweet/component-aws-s3@1.2.1) (2018-08-17)




**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="1.2.0"></a>
# [1.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.1.2...@pubsweet/component-aws-s3@1.2.0) (2018-08-02)


### Features

* **aws-s3:** add option to save files with new names ([14e66e3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/14e66e3))




<a name="1.1.2"></a>
## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.1.1...@pubsweet/component-aws-s3@1.1.2) (2018-06-19)


### Bug Fixes

* add content-type to delete file middleware ([9fd2d92](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9fd2d92))




<a name="1.1.1"></a>
## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.1.0...@pubsweet/component-aws-s3@1.1.1) (2018-06-01)




**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="1.1.0"></a>
# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.0.4...@pubsweet/component-aws-s3@1.1.0) (2018-05-24)


### Bug Fixes

* **aws-s3:** add end when sending 204 ([5a72d40](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5a72d40))
* **aws-s3:** handle case when no files were found ([36241c2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/36241c2))


### Features

* **aws-s3:** file download and select zip file types ([e4a876f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e4a876f))




<a name="1.0.4"></a>
## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.0.3...@pubsweet/component-aws-s3@1.0.4) (2018-04-24)


### Bug Fixes

* **aws-s3:** update getSigned url ([d6f27ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d6f27ef))




<a name="1.0.3"></a>
## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.0.2...@pubsweet/component-aws-s3@1.0.3) (2018-04-03)




**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="1.0.2"></a>
## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.0.1...@pubsweet/component-aws-s3@1.0.2) (2018-03-27)




**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="1.0.1"></a>
## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@1.0.0...@pubsweet/component-aws-s3@1.0.1) (2018-03-19)




**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@0.1.1...@pubsweet/component-aws-s3@1.0.0) (2018-03-15)


### Features

* **aws-s3:** add endpoint to zip S3 files of a manuscript ([f50f602](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f50f602))


### BREAKING CHANGES

* **aws-s3:** renamed the AWS-S3 endpoints to conform to REST principles (pluralize entity name)




<a name="0.1.1"></a>

## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@0.1.0...@pubsweet/component-aws-s3@0.1.1) (2018-02-16)

**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="0.1.0"></a>

# 0.1.0 (2018-02-08)

### Features

* **components:** added aws s3 ([73c0764](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/73c0764))
