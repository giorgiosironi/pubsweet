# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [5.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@5.0.2...@pubsweet/coko-theme@5.0.3) (2018-12-12)

**Note:** Version bump only for package @pubsweet/coko-theme





## [5.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@5.0.1...@pubsweet/coko-theme@5.0.2) (2018-12-04)

**Note:** Version bump only for package @pubsweet/coko-theme





## [5.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@5.0.0...@pubsweet/coko-theme@5.0.1) (2018-11-30)

**Note:** Version bump only for package @pubsweet/coko-theme





# [5.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@4.1.0...@pubsweet/coko-theme@5.0.0) (2018-11-29)


### Features

* **coko-theme:** upgrade styled-components ([a4d03a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a4d03a0))


### BREAKING CHANGES

* **coko-theme:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="4.1.0"></a>
# [4.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@4.0.2...@pubsweet/coko-theme@4.1.0) (2018-09-04)


### Bug Fixes

* **menu:** add style to menu multiple ([7964081](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7964081))


### Features

* **formbuilder:** add validation for elements ([882935a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/882935a))




<a name="4.0.2"></a>
## [4.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@4.0.1...@pubsweet/coko-theme@4.0.2) (2018-08-02)




**Note:** Version bump only for package @pubsweet/coko-theme

<a name="4.0.1"></a>
## [4.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@4.0.0...@pubsweet/coko-theme@4.0.1) (2018-07-09)




**Note:** Version bump only for package @pubsweet/coko-theme

<a name="4.0.0"></a>
# [4.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@3.0.1...@pubsweet/coko-theme@4.0.0) (2018-07-02)


### Bug Fixes

* **coko-theme:** update font size and line height values ([5e7da03](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5e7da03))


### Features

* **ui:** introduce more line height variables ([85c24e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85c24e2))


### BREAKING CHANGES

* **ui:** the existing fontLineHeight variable is gone and replaced by multiple new variables




<a name="3.0.1"></a>
## [3.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@3.0.0...@pubsweet/coko-theme@3.0.1) (2018-06-28)




**Note:** Version bump only for package @pubsweet/coko-theme

<a name="3.0.0"></a>
# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@2.1.0...@pubsweet/coko-theme@3.0.0) (2018-06-28)


### Code Refactoring

* **ui:** replace current gridunit variables with one small value ([cf48f29](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cf48f29))


### Features

* **ui:** reintroduce warning color ([27943ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/27943ad))


### BREAKING CHANGES

* **ui:** Your ui components will now be multiplying a much smaller value so they need to be
adjusted




<a name="2.1.0"></a>
# [2.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@2.0.0...@pubsweet/coko-theme@2.1.0) (2018-06-19)


### Bug Fixes

* **coko theme:** improve appbar ui for coko theme ([9bd07b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9bd07b0))


### Features

* **ui-toolkit:** reuse animations ([be962dd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be962dd))




<a name="2.0.0"></a>
# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@1.0.1...@pubsweet/coko-theme@2.0.0) (2018-06-01)


### Features

* **ui:** add darken-lighten functions to toolkit ([ba8ab1b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba8ab1b))
* **ui:** add scaling function to ui-toolkit ([c92fe32](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c92fe32))
* **ui:** start ui-toolkit module ([2083b9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2083b9c))


### BREAKING CHANGES

* **ui:** th now comes from the toolkit, so all th imports from ui are now broken




<a name="1.0.1"></a>
## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@1.0.0...@pubsweet/coko-theme@1.0.1) (2018-05-09)


### Bug Fixes

* **theme:** fix active underline for action on coko theme ([9ef7165](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9ef7165))




<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@0.1.1...@pubsweet/coko-theme@1.0.0) (2018-05-03)


### Bug Fixes

* **theme:** enable boxShadow, even if it is not currently used anywhere ([f2c5538](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f2c5538))
* **theme:** remove hardcoded color ([4786945](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4786945))
* **theme:** remove warning color ([c0897c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c0897c8))
* **theme:** update theme colors ([a32b92f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a32b92f))
* **xpub-dashboard:** correct styles for author manuscripts ([1d8761e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1d8761e))


### Features

* **theme:** coko theme is in place ([731f501](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/731f501))
* **ui:** add action element ([301d800](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/301d800))
* **ui:** add action group ([32b9555](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/32b9555))


### BREAKING CHANGES

* **theme:** might break components that used the warning colors




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/coko-theme@0.1.0...@pubsweet/coko-theme@0.1.1) (2018-04-11)




**Note:** Version bump only for package @pubsweet/coko-theme

<a name="0.1.0"></a>
# 0.1.0 (2018-03-28)


### Features

* **theme:** add a Coko theme ([0c503cf](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0c503cf))
