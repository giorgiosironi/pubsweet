ActionGroup is a list of Actions displayed inline

```js
<ActionGroup>
  <Action to="/edit">Edit</Action>
  <Action>Rename</Action>
  <Action>Delete</Action>
</ActionGroup>
```
