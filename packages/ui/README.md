To do list for UI components:

* Use `data-test-id` so that we're able to easily target components in tests (#416)
* Remove hardcoded references to react-feather. Icons should come with the theme (#411)
* Add proptypes to all components
* Add tests to all components
* Apply theme overrides wherever applicable
* Use validation color function from toolkit wherever applicable
* Update components so that they don't define their position in their parent components (eg. margin around them)
* Standardize how components are exported
* Add automated accessibility tests
* Define UI component contribution guide
