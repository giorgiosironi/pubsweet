# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.1.3...pubsweet-server@10.1.4) (2018-12-12)

**Note:** Version bump only for package pubsweet-server





## [10.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.1.2...pubsweet-server@10.1.3) (2018-12-04)

**Note:** Version bump only for package pubsweet-server





## [10.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.1.1...pubsweet-server@10.1.2) (2018-11-30)

**Note:** Version bump only for package pubsweet-server





## [10.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.1.0...pubsweet-server@10.1.1) (2018-11-29)


### Bug Fixes

* add waait dependency ([f80dbec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f80dbec))





      <a name="10.1.0"></a>
# [10.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.0.3...pubsweet-server@10.1.0) (2018-11-13)


### Bug Fixes

* add a custom async server.close function ([bfc78a8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bfc78a8))
* close server and stop job queues on server close ([b20a837](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b20a837))
* manage closing of startServer in tests ([e247261](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e247261))


### Features

* initial commit for pg-boss based job queue ([6f7a0cc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6f7a0cc))
* start job queue when server starts ([5b363d8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5b363d8))
* support for a job runner in a different process ([f3c8b93](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f3c8b93))
* working job roundtrip and test ([188f794](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/188f794))
* **server:** expand access to custom models in authsome ([6fde33a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6fde33a))




      <a name="10.0.3"></a>
## [10.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.0.2...pubsweet-server@10.0.3) (2018-11-05)


### Bug Fixes

* **server:** wait before closing pubsub database connection ([badd6e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/badd6e2))




<a name="10.0.2"></a>
## [10.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.0.1...pubsweet-server@10.0.2) (2018-10-17)


### Bug Fixes

* **authenticate:** remove passwordHash from login response ([39e854c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/39e854c))
* **authentication:** filter passwordHash via authsome ([8ccc747](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8ccc747))




<a name="10.0.1"></a>
## [10.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@10.0.0...pubsweet-server@10.0.1) (2018-10-08)




**Note:** Version bump only for package pubsweet-server

<a name="10.0.0"></a>
# [10.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@9.2.0...pubsweet-server@10.0.0) (2018-09-29)


### Features

* add global property to team ([81b2a7b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/81b2a7b)), closes [#424](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/424)


### BREAKING CHANGES

* Teams now have a built-in global property meant to distinguish between object-based
teams and global teams. Previously a global team was defined as a team missing its object, but since
that is ambiguous, i.e. an object can be missing for several reasons - a global property is much
more straight-forward. If you were already using a .global property on Team in your app, your app
will break as the GraphQL schema will conflict.




<a name="9.2.0"></a>
# [9.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@9.1.2...pubsweet-server@9.2.0) (2018-09-28)


### Features

* add destroy method to graphql pubsub ([0c51f41](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0c51f41))
* use [@pubsweet](https://gitlab.coko.foundation/pubsweet)/logger for graphql errors ([2c031fe](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2c031fe))




<a name="9.1.2"></a>
## [9.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@9.1.1...pubsweet-server@9.1.2) (2018-09-27)


### Bug Fixes

* **server:** get model from the component ([ce88a8c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ce88a8c))
* **server:** require components relatively ([b2ae124](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b2ae124))




<a name="9.1.1"></a>
## [9.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@9.1.0...pubsweet-server@9.1.1) (2018-09-27)




**Note:** Version bump only for package pubsweet-server

<a name="9.1.0"></a>
# [9.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@9.0.0...pubsweet-server@9.1.0) (2018-09-25)


### Bug Fixes

* **server:** add missing objection dependency ([3dfdae3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3dfdae3))
* **server:** specify db-manager dev dependency for server ([b86a564](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b86a564))


### Features

* **server:** add configurable token expiration ([f2eb1a8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f2eb1a8)), closes [#427](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/427)




<a name="9.0.0"></a>
# [9.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@8.2.0...pubsweet-server@9.0.0) (2018-09-20)


### Features

* add base-model package for standalone data models ([fc446e8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fc446e8)), closes [#395](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/395)


### BREAKING CHANGES

* In PubSweet server, the exported thing is no longer startServer directly, but it's
now part of the exported object. This will break applications that use the equivalent of const
startServer = require('pubsweet-server'). The new method for getting a startServer is const {
startServer } = require('pubsweet-server').




<a name="8.2.0"></a>
# [8.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@8.1.0...pubsweet-server@8.2.0) (2018-09-19)


### Features

* **server:** remove require-relative ([38a8f50](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/38a8f50))




<a name="8.1.0"></a>
# [8.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@8.0.1...pubsweet-server@8.1.0) (2018-09-04)


### Features

* **client:** add AuthorizeWithGraphQL ([57eca9a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/57eca9a))




<a name="8.0.1"></a>
## [8.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@8.0.0...pubsweet-server@8.0.1) (2018-08-20)


### Bug Fixes

* **server:** use TeamInput for updateTeam mutation too ([b84b369](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b84b369))




<a name="8.0.0"></a>
# [8.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@7.2.0...pubsweet-server@8.0.0) (2018-08-17)


### Bug Fixes

* **graphql:** fix bug where frontend gets bad data on subscription ([3301269](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3301269))
* **graphql:** fix error that would crash the server on bad auth token ([058c6cd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/058c6cd))
* **graphql:** return one pubsub only in getPubsub ([d016272](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d016272))
* **graphql:** use same db client in pubsub connection ([f002f4a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f002f4a))
* **graphql:** use same version of graphql across the project ([445f204](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/445f204))
* **server:** use appropriate inputs for GraphQL mutations ([dd6d3f4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/dd6d3f4)), closes [#418](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/418)
* **server:** use the existing http server for subscriptions ([c5d1362](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c5d1362))


### Features

* **graphql:** add subscription support to graphql ([d71b0c6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d71b0c6))
* **graphql:** add upload progress subscription definition ([4248ffd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4248ffd))
* **graphql:** enable authentication over the websocket ([98c9e6d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98c9e6d))
* **graphql:** get hostname from variables ([64b7c4f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/64b7c4f))
* **graphql:** replace redis pubsub with postgres pubsub ([7c3ab7c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c3ab7c))
* **graphql:** use only one pubsub object on the server ([a875e77](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a875e77))


### BREAKING CHANGES

* **server:** Inputs are no longer strings, they are now objects.




<a name="7.2.0"></a>
# [7.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@7.1.2...pubsweet-server@7.2.0) (2018-07-09)


### Features

* **server:** make morgan request log format configurable ([a2a3810](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a2a3810))




<a name="7.1.2"></a>
## [7.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@7.1.1...pubsweet-server@7.1.2) (2018-06-19)




**Note:** Version bump only for package pubsweet-server

<a name="7.1.1"></a>
## [7.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@7.1.0...pubsweet-server@7.1.1) (2018-06-01)


### Bug Fixes

* **styleguide:** add and use simple authsome mode in styleguide ([e2e0e85](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e2e0e85))




<a name="7.1.0"></a>
# [7.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@7.0.1...pubsweet-server@7.1.0) (2018-05-18)


### Bug Fixes

* use one file at monorepo root ([456f49b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/456f49b))


### Features

* migration runner ([be49be5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be49be5))




<a name="7.0.1"></a>
## [7.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@7.0.0...pubsweet-server@7.0.1) (2018-05-03)


### Bug Fixes

* **server:** update nested PATCH endpoint ([9793075](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9793075))




<a name="7.0.0"></a>
# [7.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@6.1.0...pubsweet-server@7.0.0) (2018-05-03)


### Features

* add config property for graphql endpoint ([c463855](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c463855))
* **server:** add current and update state to update authorization ([9b2b073](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b2b073)), closes [#393](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/393)


### BREAKING CHANGES

* **server:** Authsome mode parts that deal with 'PATCH' or update operations, must now deal with
the changed API, which provides a {current:, update:} object, where 'current' represents the current
state of the object, and 'update' representes the requested update. This was added so that one can
verify e.g. that a object.status update is allowed, if the current object.status is 'editing', and
the wished update.status is 'edited'.




<a name="6.1.0"></a>
# [6.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@6.0.0...pubsweet-server@6.1.0) (2018-04-25)


### Features

* **server:** add sse events to team endpoints ([26a739a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/26a739a)), closes [#390](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/390)




<a name="6.0.0"></a>
# [6.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@5.0.0...pubsweet-server@6.0.0) (2018-04-24)


### Bug Fixes

* **server:** correct 'updated' property to 'update' ([18a6b35](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/18a6b35)), closes [#385](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/385)


### Features

* **server:** implement authorization-based filtering of SSE ([a1b5cd3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a1b5cd3))
* **server:** simplify currentUser query response ([6432b58](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6432b58))


### BREAKING CHANGES

* **server:** remove `token` from `currentUser` query response since it must have been provided in the request.




<a name="5.0.0"></a>
# [5.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@4.0.0...pubsweet-server@5.0.0) (2018-04-11)


### Bug Fixes

* **graphql:** make teamType a string ([d2005ac](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d2005ac))
* **pubsweet-server:** typo correction ([ed88381](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ed88381))
* **pubsweet-server:** update teams api ([6652307](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6652307))
* **teams:** add object.team to authsome query ([79b39b5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/79b39b5))
* **teams:** fix test authsome mode for PATCH ([f448c19](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f448c19))


### BREAKING CHANGES

* **pubsweet-server:** The teams API endpoints now work differently in terms of filtering capabilities & in combination with Authsome, in the same way as the collections and fragments API endpoints work. See https://gitlab.coko.foundation/pubsweet/pubsweet/merge_requests/177 for more information.




<a name="4.0.0"></a>
# [4.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@3.0.0...pubsweet-server@4.0.0) (2018-04-03)


### Features

* **server:** remove user.teams pre-filling ([055eafe](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/055eafe))


### BREAKING CHANGES

* **server:** User.teams no longer returns an array of objects, but instead returns an array of
ids, as it's stored in the database.




<a name="3.0.0"></a>
# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@2.0.5...pubsweet-server@3.0.0) (2018-03-30)


### Bug Fixes

* **graphql:** reorder authorization checks for creation ([25c6274](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/25c6274))
* **server:** add create property validation ([0eb3f4a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0eb3f4a))
* **server:** add nesting back to users list endpoint ([da01334](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/da01334))


### Features

* **server:** add permission based filtering to users api ([f440388](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f440388))
* **server:** make team types strings ([1fee5f0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1fee5f0))


### BREAKING CHANGES

* **server:** Team's teamType is now a string (e.g. 'managingEditor', 'reviewer')instead of an object.




<a name="2.0.5"></a>
## [2.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@2.0.4...pubsweet-server@2.0.5) (2018-03-28)


### Bug Fixes

* set owners when creating entities via GraphQL API ([992a1c2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/992a1c2))




<a name="2.0.4"></a>
## [2.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@2.0.3...pubsweet-server@2.0.4) (2018-03-27)




**Note:** Version bump only for package pubsweet-server

<a name="2.0.3"></a>
## [2.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@2.0.2...pubsweet-server@2.0.3) (2018-03-19)


### Bug Fixes

* **server:** cast booleans to string when filtering ([25f55c3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/25f55c3))




<a name="2.0.2"></a>
## [2.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@2.0.1...pubsweet-server@2.0.2) (2018-03-15)




**Note:** Version bump only for package pubsweet-server

<a name="2.0.1"></a>

## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@2.0.0...pubsweet-server@2.0.1) (2018-03-09)

### Bug Fixes

* **server:** return file url from upload mutation ([e10a10f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e10a10f))

<a name="2.0.0"></a>

# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@1.1.1...pubsweet-server@2.0.0) (2018-02-23)

### Features

* **server:** GraphQL endpoint improvements ([6b2858c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6b2858c))
* **server:** upload handling via GraphQL ([15b92e0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/15b92e0))
* switch to PostgreSQL ([d459299](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d459299))

### BREAKING CHANGES

* All data is now persisted in a PostgreSQL database instead of PouchDB
* Database server must be running and have an existing database before running `pubsweet setupdb` (Docker config provided)
`pubsweet start` runs `npm start` script if found and falls back to `pubsweet server`
`pubsweet server` starts the PubSweet server (like the old `pubsweet start`)
`pubsweet-server` model API is unchanged
* **server:** introduce pubsweet-server.uploads config value to specify location of uploaded files
Split GraphQL endpoint tests into separate files
Small refactor of api helper

<a name="1.1.1"></a>

## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@1.1.0...pubsweet-server@1.1.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-server

<a name="1.1.0"></a>

# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-server@1.0.7...pubsweet-server@1.1.0) (2018-02-02)

### Features

* **server:** add GraphQL endpoint with basic queries ([71383e3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/71383e3)), closes [#317](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/317)
