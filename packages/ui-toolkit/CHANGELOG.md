# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@2.0.2...@pubsweet/ui-toolkit@2.0.3) (2018-12-12)

**Note:** Version bump only for package @pubsweet/ui-toolkit





## [2.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@2.0.1...@pubsweet/ui-toolkit@2.0.2) (2018-12-04)

**Note:** Version bump only for package @pubsweet/ui-toolkit





## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@2.0.0...@pubsweet/ui-toolkit@2.0.1) (2018-11-30)

**Note:** Version bump only for package @pubsweet/ui-toolkit





# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@1.2.0...@pubsweet/ui-toolkit@2.0.0) (2018-11-29)


### Features

* **various:** upgrade styled-components ([9b886f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b886f6))


### BREAKING CHANGES

* **various:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="1.2.0"></a>
# [1.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@1.1.3...@pubsweet/ui-toolkit@1.2.0) (2018-08-02)


### Bug Fixes

* **ui-toolkit:** fix broken th import ([c8f8ba4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c8f8ba4))


### Features

* **ui-toolkit:** add borderColor function to theme helper ([0ed3fc1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0ed3fc1))




<a name="1.1.3"></a>
## [1.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@1.1.2...@pubsweet/ui-toolkit@1.1.3) (2018-07-09)


### Bug Fixes

* **ui-toolkit:** fix Root override not getting registered ([075e439](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/075e439))




<a name="1.1.2"></a>
## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@1.1.1...@pubsweet/ui-toolkit@1.1.2) (2018-07-02)


### Bug Fixes

* **ui-toolkit:** plug holes in override function ([e7153b7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e7153b7))




<a name="1.1.1"></a>
## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@1.1.0...@pubsweet/ui-toolkit@1.1.1) (2018-06-28)


### Bug Fixes

* **ui-toolkit:** fix override so root does not have all styles ([bd5c75c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bd5c75c))




<a name="1.1.0"></a>
# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui-toolkit@1.0.0...@pubsweet/ui-toolkit@1.1.0) (2018-06-19)


### Features

* **ui-toolkit:** reuse animations ([be962dd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/be962dd))




<a name="1.0.0"></a>
# 1.0.0 (2018-06-01)


### Bug Fixes

* **toolkit-ui:** add babelrc file for compile ([6559f31](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6559f31))
* **ui-toolkit:** add babelrc file to ui toolkit ([a381573](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a381573)), closes [#398](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/398)
* **ui-toolkit:** make package public ([aaf52a1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aaf52a1))


### Features

* **ui:** add darken-lighten functions to toolkit ([ba8ab1b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba8ab1b))
* **ui:** add override shorthand for styled components ([88c4f48](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/88c4f48))
* **ui:** add scaling function to ui-toolkit ([c92fe32](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c92fe32))
* **ui:** start ui-toolkit module ([2083b9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2083b9c))


### BREAKING CHANGES

* **ui:** th now comes from the toolkit, so all th imports from ui are now broken




<a name="1.0.0"></a>
# 1.0.0 (2018-06-01)


### Bug Fixes

* **toolkit-ui:** add babelrc file for compile ([6559f31](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6559f31))
* **ui-toolkit:** add babelrc file to ui toolkit ([a381573](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a381573)), closes [#398](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/398)


### Features

* **ui:** add darken-lighten functions to toolkit ([ba8ab1b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba8ab1b))
* **ui:** add override shorthand for styled components ([88c4f48](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/88c4f48))
* **ui:** add scaling function to ui-toolkit ([c92fe32](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c92fe32))
* **ui:** start ui-toolkit module ([2083b9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2083b9c))


### BREAKING CHANGES

* **ui:** th now comes from the toolkit, so all th imports from ui are now broken
